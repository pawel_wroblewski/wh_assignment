package wh01;

import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import wh01.browsing.Browser;
import wh01.browsing.Utils;
import wh01.browsing.pages.CompetitionsPage;
import wh01.browsing.pages.MainWHPage;
import wh01.browsing.webelements.BetSlipSection;
import wh01.browsing.webelements.EventElement;

import java.math.BigDecimal;
import java.util.List;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static wh01.browsing.pages.CompetitionsPage.CompetitionsType.PREMIERSHIP;
import static wh01.browsing.webelements.BettingOption.BettingOptionType.HOME_WIN;

public class MyBet {

    private Browser browser;
    private EventElement theEvent;
    private CompetitionsPage premiershipPage;
    private BigDecimal amountPlaced;

    @Given("^I opened a \"(.*)\" version of browser$")
    public void openABrowser(String browserTypeString) {
        browser = new Browser(Browser.BrowserType.valueOf(browserTypeString));
    }

    @When("^I am logged in on WilliamHill main page with username as \"(.*)\" and password as \"(.*)\"$")
    public void goToWilliamHillMainPageAndLogin(String username, String password) {

        MainWHPage mainWHPage = browser.navigateToMainWHPage();

        if (!mainWHPage.isLoggedIn())
            mainWHPage.logIn(username, password);

        assertThat(mainWHPage.isLoggedIn(), is(equalTo(true)));
    }

    @And("^I navigate to a premiership event$")
    public void navigateToPremiershipEvent() {
        premiershipPage = browser.navigateToCompetitionsPage(PREMIERSHIP);
        theEvent = premiershipPage
                .getEvents()
                .stream()
                .findFirst()
                .orElseThrow(() -> new AssertionError("No premiership football event found on the page."));

        assertThat(theEvent, is(not(nullValue(EventElement.class))));
    }

    @And("^I select event and place a £ \"(.*)\" bet for the home team to ‘Win’$")
    public void selectTheEventAndPutBetAmount(String amount) {
        theEvent.selectBettingOption(HOME_WIN);

        BetSlipSection betSlipSection = premiershipPage.getBetSlipSection();
        if (!betSlipSection.isDisplayed()) {
            betSlipSection.display();
        }
        assertThat(betSlipSection.isDisplayed(), is(equalTo(true)));

        betSlipSection.enterAmount(new BigDecimal(amount));
        amountPlaced = new BigDecimal(amount);

    }

    @And("^I place bet and assert the odds and returns offered$")
    public void placeBetAndAssertTheOddsAndReturnsOffered() {
        BetSlipSection betSlipSection = premiershipPage.getBetSlipSection();

        BetSlipSection.OfferDetails offerDetails = betSlipSection.getOfferDetails();
        assertThat(offerDetails.getAmountPlaced(), is(equalTo(amountPlaced)));

        List<Integer> odds = theEvent.getSelectedOdds(HOME_WIN);
        BigDecimal amountToReturn = Utils.evaluateAmountToReturn(odds, amountPlaced);
        assertThat(offerDetails.getAmountToReturn(), is(equalTo(amountToReturn)));

        betSlipSection.placeBet();
    }

    @Then("^The bet is placed$")
    public void checkPass() {

        BetSlipSection betSlipSection = premiershipPage.getBetSlipSection();
        boolean finalResult = !betSlipSection.isErrorVisible() && betSlipSection.isConfirmationNoticeVisible();
        assertThat("Is bet placed? ", finalResult, is(equalTo(true)));

    }

    @After
    public void cleanup() {
        browser.close();
    }

}
