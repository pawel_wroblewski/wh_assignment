Feature: MyBet

  Scenario Outline:
    Given I opened a "<browser>" version of browser
    When I am logged in on WilliamHill main page with username as "<username>" and password as "<password>"
    And I navigate to a premiership event
    And I select event and place a £ "<amount>" bet for the home team to ‘Win’
    And I place bet and assert the odds and returns offered
    Then The bet is placed

    Examples:
      |   username   | password  | browser | amount |
      |WHITA_jiwanski|GranadaWH07| DESKTOP |  0.05  |
      |WHITA_jiwanski|GranadaWH07| MOBILE  |  0.05  |
      | WHITA_opex7  | 0p3x2017  | DESKTOP |  0.07  |

