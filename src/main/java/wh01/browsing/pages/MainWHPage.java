package wh01.browsing.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.FluentWait;

import java.util.concurrent.TimeUnit;

import static wh01.browsing.Utils.sleepFor;
import static wh01.browsing.Utils.waitOn;

public class MainWHPage {
    private WebDriver driver;

    public MainWHPage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isLoggedIn() {

        WebElement accountTabButton = waitOn(driver)
                .until(drvr -> drvr.findElement(By.id("accountTabButton")));

        boolean loggedIn = true;
        try {
            WebElement notLoggedInIcon = new FluentWait<>(accountTabButton)
                    .withTimeout(5, TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS).ignoring(NoSuchElementException.class)
                    .until(button -> button.findElement(By.cssSelector(".icon-account-nli")));
            int notLoggedInIconHeight = notLoggedInIcon.getSize().getHeight();
            if (notLoggedInIconHeight > 10)
                loggedIn = false;
        }
        catch(NoSuchElementException|TimeoutException ex) {
            loggedIn = true;
        }

        return loggedIn;
    }

    public void logIn(String username, String password) {
        waitOn(driver).until(drvr -> drvr.findElement(By.id("accountTabButton"))).click();
        waitOn(driver).until(drvr -> drvr.findElement(By.id("loginUsernameInput"))).sendKeys(username);
        waitOn(driver).until(drvr -> drvr.findElement(By.id("loginPasswordInput"))).sendKeys(password);
        waitOn(driver).until(drvr -> drvr.findElement(By.id("loginButton"))).click();

        // don't know how to skip this sleep
        // without it webdrive goes to fast checking if the user is logged in, icons are not loaded yet and the check fails
        sleepFor(3000);
    }
}
