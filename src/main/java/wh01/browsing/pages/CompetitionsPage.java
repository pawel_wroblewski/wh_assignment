package wh01.browsing.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import wh01.browsing.Browser;
import wh01.browsing.webelements.BetSlipSection;
import wh01.browsing.webelements.EventElement;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static wh01.browsing.Utils.waitOn;

public class CompetitionsPage {
    private WebDriver driver;

    public CompetitionsPage(WebDriver driver) {
        this.driver = driver;
    }
    public List<EventElement> getEvents() {

        return waitOn(driver)
                .until(drvr -> drvr.findElements(By.className("event")))
                .stream()
                .map(EventElement::new)
                .collect(toList());
    }

    public BetSlipSection getBetSlipSection() {
        WebElement betSlipSection = waitOn(driver).until(drvr -> drvr.findElement(By.id("betslip-section")));
        return new BetSlipSection(betSlipSection, driver);
    }


    public enum CompetitionsType {
        PREMIERSHIP("http://sports.williamhill.com/betting/en-gb/football/competitions/English-Premier-League");

        String url;

        public String getUrl() {
            return url;
        }

        CompetitionsType(String url) {
            this.url = url;
        }

    }
}
