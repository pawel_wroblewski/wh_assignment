package wh01.browsing;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.FluentWait;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Utils {
    public static <T> FluentWait<T> waitOn(T webElement) {
        return new FluentWait<>(webElement)
                    .withTimeout(30, TimeUnit.SECONDS)
                    .pollingEvery(1, TimeUnit.SECONDS)
                    .ignoring(NoSuchElementException.class);
    }

    public static void sleepFor(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static BigDecimal evaluateAmountToReturn(List<Integer> odds, BigDecimal amountPlaced) {
        BigDecimal oddsEnumerator = new BigDecimal(odds.get(0));
        BigDecimal oddsDenominator = new BigDecimal(odds.get(1));

        return oddsEnumerator.add(oddsDenominator).divide(oddsDenominator).multiply(amountPlaced).setScale(2, BigDecimal.ROUND_HALF_DOWN);
    }
}
