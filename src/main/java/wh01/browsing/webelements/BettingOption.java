package wh01.browsing.webelements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static wh01.browsing.Utils.waitOn;

public class BettingOption {

    private WebElement bettingSelection;

    BettingOption(WebElement bettingSelection) {
        this.bettingSelection = bettingSelection;
    }

    public List<Integer> getSelectedOdds() {
        WebElement btn_betbutton_oddsbutton = waitOn(bettingSelection)
                .until(betSelection -> betSelection.findElement(By.className("betbutton")));
        String oddsText = waitOn(btn_betbutton_oddsbutton)
                .until(betbutton -> betbutton.findElement(By.cssSelector(".betbutton__odds")))
                .getText();

        return stream(oddsText.split("/"))
                .map(Integer::valueOf)
                .collect(toList());
    }

    public void select() {
        waitOn(bettingSelection)
                .until(betSelection -> betSelection.findElement(By.className("betbutton")))
                .click();
                // the above does not work for mobile emulation.
    }

    public enum BettingOptionType {
        HOME_WIN, DRAW, AWAY_WIN
    }
}
