package wh01.browsing.webelements;

import org.openqa.selenium.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static wh01.browsing.Utils.sleepFor;
import static wh01.browsing.Utils.waitOn;
import static org.hamcrest.MatcherAssert.assertThat;


public class BetSlipSection {
    private static final String betAmountInputCssSelector = ".betslip-selection__stake-input.ng-pristine.ng-untouched.ng-valid.betslip-selection__stake-input--visible.betslip-selection__stake-input--desktop";

    private final WebElement betSlipSection;
    private final WebDriver driver;

    public BetSlipSection(WebElement betSlipSection, WebDriver driver) {
        this.betSlipSection = betSlipSection;
        this.driver = driver;
    }

    public boolean isDisplayed() {
        //return this.betSlipSection.isDisplayed();
        return this.betSlipSection.getSize().getHeight() > 100;
    }

    public void display() {
        WebElement betSlipButtonToolbar = waitOn(driver).until(drvr -> drvr.findElement(By.id("betslip-btn-toolbar")));
        WebElement toggleBetslip = waitOn(betSlipButtonToolbar)
                .until(buttonToolbar -> buttonToolbar.findElement(By.className("toggle-betslip")));
        toggleBetslip.click();
        waitOn(betSlipSection).until(slipSection -> slipSection.findElement(By.cssSelector(betAmountInputCssSelector)));
    }

    public void enterAmount(BigDecimal bigDecimal) {
        WebElement betAmountInput = waitOn(betSlipSection).until(section -> section.findElement(By.cssSelector(betAmountInputCssSelector)));
        betAmountInput.sendKeys(bigDecimal.toString());
    }

    public void placeBet() {
        WebElement placeBetButton = waitOn(betSlipSection).until(section -> section.findElement(By.cssSelector(".o-btn.o-btn--primary.js-place-bet-button")));
        placeBetButton.click();

        // don't know how to skip this sleep
        // without it webdriver goes too fast and checking if the bet is placed fails
        sleepFor(3000);

    }

    public OfferDetails getOfferDetails() {
        String totalStakePriceId = "total-stake-price";
        String totalToReturnPriceId = "total-to-return-price";

        String totalStakePrice = waitOn(betSlipSection).until(section -> section.findElement(By.id(totalStakePriceId))).getText();
        String totalToReturnPrice = waitOn(betSlipSection).until(section -> section.findElement(By.id(totalToReturnPriceId))).getText();

        List<BigDecimal> offerDetails = Stream.of(totalStakePrice, totalToReturnPrice)
                .map(BigDecimal::new)
                .collect(Collectors.toList());
        return new OfferDetails(offerDetails);
    }


    public static class OfferDetails {
        private List<BigDecimal> offerDetails;

        OfferDetails(List<BigDecimal> offerDetails) {
            this.offerDetails = offerDetails;
            assertThat(offerDetails.size(), is(equalTo(2)));
        }

        public BigDecimal getAmountPlaced() {
            return offerDetails.get(0);
        }

        public BigDecimal getAmountToReturn() {
            return offerDetails.get(1);
        }
    }

    public boolean isErrorVisible() {

        WebElement betSlipFooterActions = waitOn(betSlipSection).until(section -> section.findElement(By.id("betslip-footer-actions")));
        boolean isError = false;
        try {
            WebElement errorBoxHeader = waitOn(betSlipFooterActions).until(section -> section.findElement(By.id("error-box-header")));
            if (errorBoxHeader.getSize().getHeight() > 10)
                isError = true;
        } catch (NoSuchElementException | TimeoutException ex) {
            isError = false;
        }

        return isError;
    }

    public boolean isConfirmationNoticeVisible() {
        //WebElement confirmation = waitOn(betSlipSection).until(section -> section.findElement(By.id("receipt-notice-box")));
        WebElement confirmation = waitOn(betSlipSection).until(section -> section.findElement(By.cssSelector(".icon-tick.betslip-receipt__header-icon")));
        //return confirmation.isDisplayed();
        return confirmation.getSize().getHeight() > 10;
    }
}
