package wh01.browsing.webelements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class EventElement {
    private WebElement eventWebElement;
    List<BettingOption> bettingOptions;

    public EventElement(WebElement eventWebElement) {
        this.eventWebElement = eventWebElement;

        bettingOptions = this.eventWebElement.findElements(By.className("btmarket__selection"))
                .stream()
                .map(webElement -> new BettingOption(webElement))
                .collect(toList());
    }

    BettingOption getBettingOption(BettingOption.BettingOptionType type) {
        switch(type) {
            case HOME_WIN:
                return bettingOptions
                        .stream()
                        .findFirst()
                        .orElseThrow(() -> new AssertionError("Couldn't obtain HOME_WIN betting option."));
            case DRAW:
                return bettingOptions
                        .stream()
                        .skip(1)
                        .findFirst()
                        .orElseThrow(() -> new AssertionError("Couldn't obtain DRAW betting option."));
            case AWAY_WIN:
                return bettingOptions
                        .stream()
                        .skip(2)
                        .findFirst()
                        .orElseThrow(() -> new AssertionError("Couldn't obtain AWAY_WWIN betting option."));
        }
        return null;
    }

    public List<Integer> getSelectedOdds(BettingOption.BettingOptionType type) {
        return this.getBettingOption(type).getSelectedOdds();
    }

    public void selectBettingOption(BettingOption.BettingOptionType type) {
        this.getBettingOption(type).select();
    }

}
