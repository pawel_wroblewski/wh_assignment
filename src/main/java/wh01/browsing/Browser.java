package wh01.browsing;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import wh01.browsing.pages.CompetitionsPage;
import wh01.browsing.pages.MainWHPage;

import java.util.HashMap;
import java.util.Map;

public class Browser {
    private WebDriver driver;

    public Browser(BrowserType browserType) {
        System.setProperty("webdriver.chrome.driver", "/opt/chromedriver");

        switch (browserType) {
            case DESKTOP:
                driver = new ChromeDriver();
                break;
            case MOBILE:
                Map<String, String> mobileEmulation = new HashMap<>();
                mobileEmulation.put("deviceName", "Galaxy S5");
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
                driver = new ChromeDriver(chromeOptions);
                break;
        }
    }

    public MainWHPage navigateToMainWHPage() {
        driver.navigate().to("http://sports.williamhill.com/betting/en-gb");
        return new MainWHPage(driver);
    }


    public CompetitionsPage navigateToCompetitionsPage(CompetitionsPage.CompetitionsType competitions) {
        driver.navigate().to(competitions.getUrl());
        return new CompetitionsPage(driver);
    }

    public void close() {
        driver.close();
    }

    public enum BrowserType {
        DESKTOP, MOBILE
    }

}
